package com.kerbores.aeasy;

import java.util.concurrent.TimeUnit;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.nutz.lang.Tasks;
import org.nutz.lang.Times;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.Fragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kerbores.aeasy.framwork.broadcast.BatteryBroadcastReceiver;
import com.kerbores.aeasy.framwork.broadcast.entrties.Battery;
import com.kerbores.aeasy.framwork.handler.UrlPostHandler;
import com.kerbores.aeasy.framwork.utils.AsyncUrlPoster;
import com.kerbores.aeasy.framwork.utils.CpuInfo;

public class MainActivity extends Activity {
	private static BatteryBroadcastReceiver br = new BatteryBroadcastReceiver();
	private static double bValue;
	private static Battery battery;
	private static MemoryInfo memory = new ActivityManager.MemoryInfo();;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
		registerReceiver(br, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));// 注册电量监听广播
		((ActivityManager) getSystemService(ACTIVITY_SERVICE)).getMemoryInfo(memory);//获取内存信息
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		private TextView view;
		private int count = 0;
		private long time;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			view = (TextView) rootView.findViewById(R.id.info);
			time = System.currentTimeMillis();
//			DBUtils.initDao(context,"userInfo.db");
			Tasks.scheduleAtFixedRate(new AsyncUrlPoster("http://203.93.106.130:11083/AppService/appEmp/genCode.ac", "phone=18716523565", new UrlPostHandler() {

				@Override
				public void handleMessage(Message msg) {
					bValue = br.getBatteryLeft();
					battery = br.getBattery();
					//CpuInfo.getCpu() 获取CPU状况
					view.setText("服务端响应: " + msg.getData().get("info").toString() + "\r\n访问次数: " + count + "\r\n剩余电量: " + bValue + "\r\n用时: " + (System.currentTimeMillis() - time)
							+ " 毫秒" + "\r\n电池详情: " + battery + "\r\nCPU信息: " + CpuInfo.getCpu() + "\r\n内存信息: " + Json.toJson(memory, JsonFormat.nice()));// 刷新界面
					count++;
				}
			}), Times.now(), 100, TimeUnit.MILLISECONDS);
			return rootView;
		}
	}

}
