package com.kerbores.aeasy.framwork.broadcast;

import com.kerbores.aeasy.framwork.broadcast.entrties.Battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BatteryBroadcastReceiver extends BroadcastReceiver {

	private Battery battery;

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
			int level = intent.getIntExtra("level", 0);
			int scale = intent.getIntExtra("scale", 100);
			battery = new Battery(level, scale, intent.getIntExtra("status", 0), intent.getIntExtra("health", 0), intent.getBooleanExtra("present", false), intent.getIntExtra(
					"icon-small", 0), intent.getIntExtra("plugged", 0), intent.getIntExtra("voltage", 0), intent.getIntExtra("temperature", 0), intent.getCharSequenceExtra(
					"technology").toString());
		}
	}

	public double getBatteryLeft() {
		return battery.getLeft();
	}

	public Battery getBattery() {
		return battery;
	}

}
