package com.kerbores.aeasy.framwork.broadcast.entrties;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;

import android.os.BatteryManager;

/**
 * 电池描述
 * 
 * @author Kerbores
 * 
 */
public class Battery {
	/**
	 * 剩余量
	 */
	private int level;
	/**
	 * 最大量
	 */
	private int scale;
	/**
	 * 电量百分比
	 */
	private double left;
	/**
	 * 状态
	 */
	private int status;
	/**
	 * 状态描述
	 */
	@SuppressWarnings("unused")
	private String statusInfo;
	/**
	 * 健康状态
	 */
	private int health;
	/**
	 * 健康状态描述
	 */
	@SuppressWarnings("unused")
	private String healthInfo;
	/**
	 * 
	 */
	private boolean present;
	/**
	 * 图标Id
	 */
	private int iconsmallId;
	/**
	 * 插座类型
	 */
	private int plugged;
	/**
	 * 插座类型描述
	 */
	@SuppressWarnings("unused")
	private String pluggedInfo;
	/**
	 * 电压
	 */
	private int voltage;
	/**
	 * 温度
	 */
	private int temperature;
	/**
	 * 实际温度
	 */
	private double trueTemperature;

	public Battery(int level, int scale, int status, int health, boolean present, int iconsmallId, int plugged, int voltage, int temperature, String technology) {
		super();
		this.level = level;
		this.scale = scale;
		this.left = level * 100 / scale;
		this.status = status;
		this.statusInfo = getStatusInfo();
		this.health = health;
		this.healthInfo = getHealthInfo();
		this.present = present;
		this.iconsmallId = iconsmallId;
		this.plugged = plugged;
		this.pluggedInfo = getPluggedInfo();
		this.voltage = voltage;
		this.temperature = temperature;
		this.trueTemperature = temperature / 10.0;
		this.technology = technology;
	}

	/**
	 * 电池类型
	 */
	private String technology;

	public int getHealth() {
		return health;
	}

	public String getHealthInfo() {
		switch (health) {
		case BatteryManager.BATTERY_HEALTH_COLD:
			return "COLD";
		case BatteryManager.BATTERY_HEALTH_DEAD:
			return "DEAD";
		case BatteryManager.BATTERY_HEALTH_GOOD:
			return "GOOD";
		case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
			return "OVER_VOLTAGE";
		case BatteryManager.BATTERY_HEALTH_OVERHEAT:
			return "OVERHEAT";
		case BatteryManager.BATTERY_HEALTH_UNKNOWN:
			return "UNKNOWN";
		case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
			return "UNSPECIFIED_FAILUR";
		default:
			return "UNKNOWN";
		}
	}

	public int getIconsmallId() {
		return iconsmallId;
	}

	public double getLeft() {
		return left;
	}

	public int getLevel() {
		return level;
	}

	public int getPlugged() {
		return plugged;
	}

	public String getPluggedInfo() {
		switch (plugged) {
		case BatteryManager.BATTERY_PLUGGED_AC:
			return "AC";
		case BatteryManager.BATTERY_PLUGGED_USB:
			return "USB";
		case BatteryManager.BATTERY_PLUGGED_WIRELESS:
			return "WIRELESS";
		default:
			return "";
		}
	}

	public int getScale() {
		return scale;
	}

	public int getStatus() {
		return status;
	}

	public String getStatusInfo() {
		switch (status) {
		case BatteryManager.BATTERY_STATUS_CHARGING:

			return "CHARGING";
		case BatteryManager.BATTERY_STATUS_DISCHARGING:

			return "DISCHARGING";
		case BatteryManager.BATTERY_STATUS_FULL:

			return "FULL";
		case BatteryManager.BATTERY_STATUS_NOT_CHARGING:

			return "NOT_CHARGING";
		case BatteryManager.BATTERY_STATUS_UNKNOWN:

			return "UNKNOWN";

		default:
			return "UNKNOWN";
		}
	}

	public String getTechnology() {
		return technology;
	}

	public int getTemperature() {
		return temperature;
	}

	public double getTrueTemperature() {
		return trueTemperature;
	}

	public int getVoltage() {
		return voltage;
	}

	public boolean isPresent() {
		return present;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setHealthInfo(String healthInfo) {
		this.healthInfo = healthInfo;
	}

	public void setIconsmallId(int iconsmallId) {
		this.iconsmallId = iconsmallId;
	}

	public void setLeft(double left) {
		this.left = left;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setPlugged(int plugged) {
		this.plugged = plugged;
	}

	public void setPluggedInfo(String pluggedInfo) {
		this.pluggedInfo = pluggedInfo;
	}

	public void setPresent(boolean present) {
		this.present = present;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setStatusInfo(String statusInfo) {
		this.statusInfo = statusInfo;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public void setTrueTemperature(double trueTemperature) {
		this.trueTemperature = trueTemperature;
	}

	public void setVoltage(int voltage) {
		this.voltage = voltage;
	}

	@Override
	public String toString() {
		return Json.toJson(this, JsonFormat.nice());
	}

}
