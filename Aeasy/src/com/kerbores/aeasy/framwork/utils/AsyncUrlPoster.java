package com.kerbores.aeasy.framwork.utils;

import java.io.IOException;

import android.os.Bundle;
import android.os.Message;

import com.assic.utils.net.URLPoster;
import com.kerbores.aeasy.framwork.handler.UrlPostHandler;

/**
 * 异步发送URL请求
 * 
 * @author Kerbores
 * 
 */
public class AsyncUrlPoster extends Thread {
	Bundle data = new Bundle();
	private String url;
	private String query;
	private Message msg;
	private UrlPostHandler handler;

	public AsyncUrlPoster(String url, String query, UrlPostHandler handler) {
		super();
		this.setUrl(url);
		this.setQuery(query);
		this.handler = handler;
	}

	@Override
	public void run() {
		handler.sendMessage(invoke(url, query));
	}

	public Message invoke(String url, String query) {
		try {
			data.putString("info", URLPoster.simpleUTF8Post(url, query));
			msg = new Message();
			msg.setData(data);
			return msg;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Message getMessage() {
		return msg;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
