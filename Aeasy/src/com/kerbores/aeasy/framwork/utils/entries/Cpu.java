package com.kerbores.aeasy.framwork.utils.entries;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;

public class Cpu {
	private int max;
	private int min;
	private int cur;
	private String name;

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getCur() {
		return cur;
	}

	public void setCur(int cur) {
		this.cur = cur;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return Json.toJson(this, JsonFormat.nice());
	}

	public Cpu(int max, int min, int cur, String name) {
		super();
		this.max = max;
		this.min = min;
		this.cur = cur;
		this.name = name;
	}

}
