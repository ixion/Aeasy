package com.kerbores.aeasy.framwork.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MemroyInfo {
	public void getTotalMemory() {  
        String str1 = "/proc/meminfo";  
        String str2="";  
        BufferedReader localBufferedReader = null;
        try {  
            FileReader fr = new FileReader(str1);  
            localBufferedReader = new BufferedReader(fr, 8192);  
            while ((str2 = localBufferedReader.readLine()) != null) {  
               System.out.println(str2);
            }  
        } catch (IOException e) {  
        	e.printStackTrace();
        }  finally{
        	try {
				localBufferedReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    } 
}
