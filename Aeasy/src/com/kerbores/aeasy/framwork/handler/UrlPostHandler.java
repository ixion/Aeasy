package com.kerbores.aeasy.framwork.handler;

import android.os.Handler;
import android.os.Message;

/**
 * URL请求响应handler
 * 
 * @author Kerbores
 * 
 */
public abstract class UrlPostHandler extends Handler {

	@Override
	public abstract void handleMessage(Message msg);
}
